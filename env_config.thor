class Set < Thor
  include Thor::Actions
  
  no_tasks do
    def copy_environment(new_environment)
      run("cp features/support/envs/env.rb.#{new_environment} features/support/env.rb")
    end
  end
  
  desc "headless", "Use Headless Chrome"
  def headless
    puts "Run cukes with Headless Chrome"
    copy_environment("headless")
  end

  desc "chrome", "Run cukes with Chrome"
  def chrome
    puts "Run cukes with Chrome Selenium"
    copy_environment("chrome")
  end

  desc "firefox", "Run cukes with Firefox"
  def firefox
    puts "Run cukes with Firefox Selenium"
    copy_environment("firefox")
  end

  desc "safari", "Run cukes with Safari"
  def safari
    puts "Run cukes with Safari Selenium"
    copy_environment("safari")
  end

  desc "edit", "Edit cukes with Chrome"
  def edit
    puts "Edit cukes with Chrome Selenium"
    copy_environment("edit")
  end

  desc "local", "Run cukes against localhost with Headless Chrome"
  def local
    puts "Run cukes against localhost with Headless Chrome"
    copy_environment("local")
  end

  desc "qa1", "Run cukes against qa1 with Headless Chrome"
  def qa1
    puts "Run cukes against qa1 with Headless Chrome"
    copy_environment("qa1")
  end

  desc "qa2", "Run cukes against qa2 with Headless Chrome"
  def qa2
    puts "Run cukes against qa2 with Headless Chrome"
    copy_environment("qa2")
  end

  desc "qa3", "Run cukes against qa3 with Headless Chrome"
  def qa3
    puts "Run cukes against qa3 with Headless Chrome"
    copy_environment("qa3")
  end
end