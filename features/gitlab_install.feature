@installs
Feature: As a new user, I'd like to learn more about Gtilab Features

Scenario: I should be able to follow Ubuntu install instructions
  Given I am on the gitlab install page
  When I select the Ubuntu section
  Then I should be on the ubuntu page
  And I verify the install instructions

Scenario: I should be able to follow Debian install instructions
  Given I am on the gitlab install page
  When I select the Debian section
  Then I verify the install instructions