Given /I am on the gitlab home page/i do
  @browser.get(@domain)
end

When /I search for "(.*)"/i do |criteria|
  @search.icon.click
  sleep 1
  @search.search = criteria
  @browser.action.send_keys(:enter).perform
  @search.first_result.click
end

