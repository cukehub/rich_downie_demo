class Features
  include PageObject

  def video_button
    @browser.first(class: 'video-button')
  end

  def video_container
    @browser.first(class: 'video-container')
  end
end