class Installs
  include PageObject

  def ubuntu
    @browser.all(class: 'tile-title')[0]
  end

  def debian
    @browser.all(class: 'tile-title')[1]
  end
end