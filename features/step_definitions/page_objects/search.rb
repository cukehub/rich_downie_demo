class Search
  include PageObject
  
  text_field(:search, class: 'search-input')
  
  def icon
    Selenium::WebDriver::Wait.new(timeout: 15).until {@browser.first(class: 'nav').first(class: 'search-icon').displayed?}
    @browser.first(class: 'nav').first(class: 'search-icon')
  end
  
  def first_result
    Selenium::WebDriver::Wait.new(timeout: 15).until {@browser.all(class: 'st-ui-result')[0].displayed?}
    @browser.all(class: 'st-ui-result')[0]
  end
end