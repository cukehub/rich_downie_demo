Given /I am on the gitlab features page/i do
  @browser.get(@domain + '/features')
end

When /I watch the demo/i do
  @features.video_button.click
end

Then /I should be able to start the video/i do
  @features.video_container.click
end