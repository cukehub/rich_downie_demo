Feature: As a new user, I'd like to learn more about Gitlab Products

@products
Scenario: I should be able to view the Gitlab products
  Given I am on the gitlab products page
  Then I should see "Community Edition (CE)" on the page
  Then I should see "Starter" on the page
  Then I should see "Premium" on the page
  Then I should see "Ultimate" on the page