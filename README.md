### Manual Testers

1. Sign Up at [CukeHub](https://cukehub.com/users/sign_up) 
2. Select ```Manual Tester``` as the Amigo Role.

### Product Owners

1. Sign Up at [CukeHub](https://cukehub.com/users/sign_up) 
2. Select ```Product Owner``` as the Amigo Role.

### Automated Testers / Developers

1. Sign Up at [CukeHub](https://cukehub.com/users/sign_up)
2. Select ```Automated Tester / Developer``` as the Amigo Role.
3. Install [Ruby](http://davehaeffner.com/selenium-guidebook/install/ruby/) unless ```ruby -v ``` exists.
4. Install [Chrome](https://www.google.com/chrome/browser/desktop/index.html) browser.
5. ```git clone https://gitlab.com/cukehub/rich_downie_demo.git```
6. ```cd rich_downie_demo```
7. ```gem install bundler```
8. ```bundle install```
9. ```thor list```
10. ```thor set:headless```
11. ```cucumber features```
12. ```rake build:all```
13. Visit the [Gitlab](https://cukehub.com/apps/93) App in CukeHub and view results.

### Install Ruby
* How to Install [Ruby](http://davehaeffner.com/selenium-guidebook/install/ruby/windows/) on Windows 
* How to Install [Ruby](http://davehaeffner.com/selenium-guidebook/install/ruby/mac/10.10/) on OSX 
* How to Install [Ruby](http://davehaeffner.com/selenium-guidebook/install/ruby/linux/) on Linux 

### Resources
* [FSAR](http://fullstackautomationwithruby.com/) (FREE) Online sessions that teach you how to automation web applications with Ruby. 
* [Cucumber](https://cucumber.io/) Watch this [Demo](https://www.youtube.com/watch?v=jcufT1xVhGA&t=2s) to learn how to cucumber.
* [Selenium-Webdriver](https://rubygems.org/gems/selenium-webdriver) is a tool for writing automated tests of websites. It aims to mimic the behaviour of a real user, and as such interacts with the HTML of the application.
* [RSpec](http://rspec.info/)
* [Page Objects](https://rubygems.org/gems/page-object) Page Object DSL that works with both Watir and Selenium.
